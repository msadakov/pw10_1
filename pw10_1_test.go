package main

import (
	"fmt"
	"testing"
)

func Test_triangle(t *testing.T) {
	cases := []struct {
		ab, ac, bc float64
		msg        string
		err        error
	}{
		{0, 1, 2, "", fmt.Errorf("Введены не корректные данные: введены нули")},
		{-1, 3, 3, "", fmt.Errorf("Введены не корректные данные: введены отрицательные числа")},
		{1, 2, 4, "", fmt.Errorf("Введены не корректные данные: это не треугольник")},
		{2, 2, 2, "Треугольник равносторонний", nil},
		{2, 3, 2, "Треугольник равнобедренный", nil},
		{3, 4, 5, "Треугольник прямоугольный", nil},
		{1, 2, 3, "Треугольник вырожденный", nil},
		{2, 4, 5, "Треугольник произвольный", nil},
	}

	for _, c := range cases {
		got1, got2 := triangle(c.ab, c.ac, c.bc)
		if got1 != c.msg && got2 != c.err {
			t.Errorf(
				"triangle(%v, %v, %v) == (%q, %q), want: (%q, %q)",
				c.ab, c.ac, c.bc, got1, got2, c.msg, c.err,
			)
		}
	}
}
