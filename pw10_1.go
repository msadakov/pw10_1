package main

import (
	"fmt"
	"math"
	"os"
)

var (
	ab, ac, bc float64
)

func printErrorAndExit(err error) {
	fmt.Fprintf(os.Stderr, "\r%s\n", err)
	os.Exit(1)
}

func triangle(ab, ac, bc float64) (msg string, err error) {
	msg, err = "", nil

	if ab == 0 || ac == 0 || bc == 0 {
		err = fmt.Errorf("Введены не корректные данные: введены нули")
	} else if ab < 0 || ac < 0 || bc < 0 {
		err = fmt.Errorf("Введены не корректные данные: введены отрицательные числа")
	} else if ac+bc < ab || ab+bc < ac || ab+ac < bc {
		err = fmt.Errorf("Введены не корректные данные: это не треугольник")
	} else if ab == ac+bc || ac == ab+bc || bc == ab+ac {
		msg = "Треугольник вырожденный"
	} else if ab == ac && bc == ac {
		msg = "Треугольник равносторонний"
	} else if ab == math.Sqrt(ac*ac+bc*bc) || ac == math.Sqrt(ab*ab+bc*bc) || bc == math.Sqrt(ab*ab+ac*ac) {
		msg = "Треугольник прямоугольный"
	} else if ab == ac || ab == bc || ac == bc {
		msg = "Треугольник равнобедренный"
	} else {
		msg = "Треугольник произвольный"
	}
	return
}

func main() {
	fmt.Printf("Введите AB:")
	fmt.Scanln(&ab)
	fmt.Printf("Введите AC:")
	fmt.Scanln(&ac)
	fmt.Printf("Введите BC:")
	fmt.Scanln(&bc)

	msg, err := triangle(ab, ac, bc)
	if err != nil {
		printErrorAndExit(err)
	}
	fmt.Println(msg)
}
